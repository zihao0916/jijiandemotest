import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	//state中是需要管理的全局变量
	state: {
		sex: "",
		userNumber: "",
		phone: "",
		openId: "",
		userId: ",",
		employeeId:"",
		myCompanyName: "",
		myCompanyId: "", //登陆的用户所在的公司
		ownCompanyUserId: "",
		userName: '',
		custmoerId: "",
		custmoerName: "未选择",
		hasLogin: false
	},
	//mutations 是操作state中变量的方法
	mutations: {
		getUserInfo() {
			const userInfo={
				sex:this.state.sex,
				userNumber: this.state.userNumber,
				phone: this.state.phone,
				openId: this.state.openId,
				userId: this.state.userId,
				myCompanyName: this.state.myCompanyName,
				myCompanyId: this.state.myCompanyId,
				ownCompanyUserId:this.state.ownCompanyUserId,
				userName:  this.state.userName,
				userType:this.state.userType,
		         employeeId:this.state.employeeId,
			}
			uni.setStorageSync('userInfo', userInfo); //把state.userInfo存储起来
	

		},
		//储存之后调用
		initUser(state) { //initUser自己随便起的名字
			const userInfo = uni.getStorageSync('userInfo')
			if(userInfo!="undefined"){
				console.log("加载缓存")
				console.log(userInfo)
			state.myCompanyName = userInfo.myCompanyName
			state.userId = userInfo.userId
			state.openId = userInfo.openId
			state.myCompanyId = userInfo.myCompanyId
			state.ownCompanyUserId = userInfo.ownCompanyUserId
			state.userName = userInfo.userName
			state.sex = userInfo.sex
			state.phone = userInfo.phone
			state.userNumber = userInfo.userNumber	
			state.userType = userInfo.userType	
			state.employeeId=userInfo.employeeId
			}else{
				console.log("不存在缓存")
			}

		},
		quit() {
			this.state.employeeId=""
			this.state.myCompanyName = ""
			this.state.userId = ""
			this.state.openId = ""
			this.state.myCompanyId = ""
			this.state.ownCompanyUserId = ""
			uni.clearStorageSync();
		},

		login(state, name) { //登录成功修改 全局变量
			state.userName = name;
			state.hasLogin = true;
		},
		loginOut(state) { //退出登录修改 全局变量
			state.userName = '';
			state.hasLogin = false;
		}
	}

})

export default store
