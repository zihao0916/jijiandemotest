import {myRequest} from 'api.js'
//登录
export function login(username, password) {
  return myRequest({
    url: 'login',
    method: 'post',
    data: {
      username,
      password
    },
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'  
	}
  })
}
