import Vue from 'vue'
import App from './App'
import uView from 'uview-ui'
import api from './api.js'
import store from './store'
Vue.prototype.$store = store
Vue.prototype.api = api

Vue.config.productionTip = false
Vue.use(uView)
App.mpType = 'app'
const app = new Vue({
	store,
    ...App
})
app.$mount()
