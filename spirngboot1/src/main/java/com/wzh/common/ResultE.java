package com.wzh.common;


public enum ResultE {
	SUCCESS(200, "操作成功"),
	ERROR(500, "服务器异常，请稍后再试"),
	USERNULL(204,"user null");


	private int code;
	private String msg;

	ResultE(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
