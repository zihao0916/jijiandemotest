package com.wzh.service;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.OrderDto;
import com.wzh.entity.po.Company;
import com.wzh.entity.po.Customer;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    DataResp<Object> isExistUserByOpenId(String id);

    DataResp<Object> bossRegister(Company company);

    DataResp<Object> myCompanyByUserId(String id);
}
