package com.wzh.service.Impl;

import com.google.common.collect.Lists;
import com.wzh.entity.dto.QueryDto;
import com.wzh.mapper.ClothesMapper;
import com.wzh.entity.po.*;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.ClotheQueryDto;
import com.wzh.entity.dto.ColorsDto;
import com.wzh.entity.dto.SizesDto;
import com.wzh.entity.vo.ClothesInfoVo;
import com.wzh.service.ClothesService;
import com.wzh.util.PyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ClothesServiceImpl implements ClothesService {
    @Resource
    private ClothesMapper clothesMapper;

    @Override
    public DataResp<ClothesDict> sizeAndColor() {
        DataResp<ClothesDict> resp = new DataResp<>();
        List<Color> colors = clothesMapper.findColor();
        List<Size> sizes = clothesMapper.findSize();

        ClothesDict clothesDict =new ClothesDict();
        clothesDict.setSizes(sizes);
        clothesDict.setColors(colors);
        resp.setData(clothesDict);
        return resp;
    }

    @Override
    public DataResp<Object> newPattern(Clothes clothes) {
        DataResp<Object> resp = new DataResp<>();
        SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmmss");
        if(clothes.getId()==null){
            clothes.setId(System.currentTimeMillis());
        }
        clothes.setClothesEncode(PyUtil.getPinyinInitial(clothes.getClothesName()+formatter.format(new Date())));
        clothesMapper.newPattern(clothes);
        return resp;
    }

    @Override
    public DataResp<Object> newTask(ClotheQueryDto clotheQueryDto) {
        DataResp<Object> resp = new DataResp<>();
        //组装参数
        List<ClothesInfo> clothesInfoList = new ArrayList<ClothesInfo>();


        //注意相同的生产任务 计算批次（未解决）
        for (ColorsDto colorsDto : clotheQueryDto.getColorsDtos()
        ) {

            for (SizesDto sizesDto:colorsDto.getSizeDtos()
                 ) {

                if(sizesDto.getMount()>0){
                    ClothesInfo clothesInfo = new ClothesInfo();
                        clothesInfo.setClothesId(clotheQueryDto.getClothesId());
                        clothesInfo.setMount(sizesDto.getMount());
                        clothesInfo.setColorName(colorsDto.getColorName());
                        clothesInfo.setColorId(colorsDto.getColorId());
                        clothesInfo.setSizeId(sizesDto.getSizeId());
                        clothesInfo.setSizeName(sizesDto.getSizeName());
                        clothesInfo.setCompanyId(clotheQueryDto.getCompanyId());

                    clothesInfoList.add(clothesInfo);
                }
            }
        }
        clothesMapper.newCode(clothesInfoList);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesByParams(String query) {
        DataResp<Object> resp = new DataResp<>();
        List<Clothes> clothesList = clothesMapper.selectClothesByParams(query);
        resp.setData(clothesList);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothes( QueryDto queryDto) {
        DataResp<Object> resp = new DataResp<>();
        Integer total = clothesMapper.selectClothesCount(queryDto);
        List<Clothes> clothesList = clothesMapper.selectClothes(queryDto);
        resp.setPageCount(total);
        resp.setData(clothesList);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesTaskById(Long id) {
        DataResp<Object> resp = new DataResp<>();
        ClothesInfoVo clothesInfoVo = clothesMapper.selectClothesTaskById(id);
        resp.setData(clothesInfoVo);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesSizeAndNum(Long id) {
        DataResp<Object> resp = new DataResp<>();

        List<colorAndNum> colorAndNumList =clothesMapper.findColorAndNum(id);

        Map<Long,String> colorMap=new HashMap<>();
        //去重 可能存在补货（相同的颜色尺码的生产任务）
        for (colorAndNum colorAndNum:colorAndNumList
             ) {
            colorAndNum.setSizeAndNumList(clothesMapper.getSizeAndNum(colorAndNum));
            colorMap.put(colorAndNum.getColorId(),colorAndNum.getColorName());
        }
        if(colorAndNumList.size()==0){
            resp.setData(null);
            return resp;
        }



        Map<Long,String> sizeMap =new HashMap<>();

        for (colorAndNum colorAndNum:colorAndNumList
        ) {
            for (SizeAndNum sizeAndNum:colorAndNum.getSizeAndNumList()
            ) {
               if(!sizeMap.containsKey(sizeAndNum.getSizeId())){
                   sizeMap.put(sizeAndNum.getSizeId(),sizeAndNum.getSizeName());
               }
            }
        }

        List<String> title = new ArrayList<>();//表头数据
        title.add("颜色/尺码");
        for(Long obj:sizeMap.keySet()){
            title.add(sizeMap.get(obj));
        }

       // Map<String, List<String>> finalData = new HashMap<>();
        List<List<String>> finalData=new ArrayList<>();


        //处理对应数据  大循环外面
        for (colorAndNum colorAndNum:colorAndNumList
        ) {
            List<String> content = Lists.newArrayList();//行数据
            content.add(colorAndNum.getColorName());

            for (int i = 1; i < title.size(); i++) {
                int count=0;
                for (SizeAndNum sizeAndNum:colorAndNum.getSizeAndNumList()
                ) {
                   if (sizeAndNum.getSizeName().equals(title.get(i))){
                       count =sizeAndNum.getRealNum();
                   }
                }
                content.add(String.valueOf(count));
            }






          //  finalData.put(colorAndNum.getColorName(),content);
            finalData.add(content);
        }


        Map<String,Object> dataResp=new HashMap<>();

        dataResp.put("title",title);
        dataResp.put("content",finalData);


        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\r\n");

        stringBuilder.append("<html><head></head><body><h2></h2>");
        stringBuilder.append("<table border=\"5\" style=\"border:solid 1px #E8F2F9;font-size=14px;;font-size:22px;\">");
        //构造html
        stringBuilder.append("<tr style=\"background-color: #428BCA; color:#ffffff\">");
        for (String titleName:title
        ) {
            stringBuilder.append("<th width=\"280px\">"+titleName+"</th>");
        }
        stringBuilder.append("</tr>");
//        stringBuilder.append("<tr style=\"background-color: #428BCA; color:#ffffff\">" +
//                "<th width=\"180px\">兑换明细名称</th>" +
//                "<th width=\"100px\">日期</th>" +
//                "<th width=\"80px\">兑换次数</th>" +
//                "<th width=\"80px\">兑换人数</th>" +
//                "<th width=\"80px\">商品单价</th>" +
//                "<th width=\"80px\">商品成本</th>" +
//                "<th width=\"80px\">消耗数</th>" +
//                "<th width=\"80px\">商品成本数</th>" +
//                "</tr>");

        for (List<String> item:finalData
        ) {
            stringBuilder.append("<tr>");
            for (String item2:item
            ) {
                stringBuilder.append("<td style=\"text-align:center\">" +item2 + "</td>");
            }
            stringBuilder.append("</tr>");
        }



        stringBuilder.append("</table>");
        stringBuilder.append("</body></html>");




        dataResp.put("html",stringBuilder);
        resp.setData(dataResp);
        return resp;
    }

    @Override
    @Transactional
    public DataResp<Object> addTaskMount(Task task) {
        DataResp<Object> resp = new DataResp<>();
        //修改库存
        clothesMapper.addTaskMount(task);
        //生成一员工上数条记录
        clothesMapper.addTaskRecord(task);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesAllTask() {
        DataResp<Object> resp = new DataResp<>();
        List<ClothesInfoVo> clothesInfoList = clothesMapper.selectClothesAllTask();
        resp.setData(clothesInfoList);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesTaskByQuery( QueryDto queryDto) {
        DataResp<Object> resp = new DataResp<>();
        if (queryDto.getIndex()!=0){
            queryDto.setIndex((queryDto.getIndex()* queryDto.getCount()));
        }
        List<ClothesInfoVo> clothesInfoList = clothesMapper.selectClothesTaskByQuery(queryDto);
        Integer count =clothesMapper.findCount(queryDto);
        resp.setData(clothesInfoList);
        resp.setPageCount(count);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesTaskByClothesId(Long id) {
        DataResp<Object> resp = new DataResp<>();
        List<ClothesInfo> clothesInfoList = clothesMapper.selectClothesTaskByClothesId(id);
        resp.setData(clothesInfoList);
        return resp;
    }

    @Override
    public DataResp<Object> showTaskMount(Task task) {
        DataResp<Object> resp = new DataResp<>();
        TaskRecord myTask = clothesMapper.showTaskMount(task);
        resp.setData(myTask);
        return resp;
    }

    @Override
    @Transactional
    public DataResp<Object> delTaskMount(TaskRecord taskRecord) {
        DataResp<Object> resp = new DataResp<>();
         clothesMapper.delTaskMount(taskRecord);
         clothesMapper.delTaskMountById(taskRecord.getTaskId(),taskRecord.getNum());
        return resp;
    }

    @Override
    public DataResp<Object> delPattern(Long id) {

        DataResp<Object> resp = new DataResp<>();
        clothesMapper.delPattern(id);
        return resp;
    }

    @Override
    public DataResp<Object> selectClothesInfo(Long id) {
        DataResp<Object> resp = new DataResp<>();
        Clothes clothes = clothesMapper.selectClothesInfo(id);
        resp.setData(clothes);
        return resp;
    }
}
