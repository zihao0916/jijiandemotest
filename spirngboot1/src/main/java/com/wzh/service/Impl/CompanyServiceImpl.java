package com.wzh.service.Impl;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.ColorsDto;
import com.wzh.entity.dto.OrderDto;
import com.wzh.entity.dto.SizesDto;
import com.wzh.entity.po.Company;
import com.wzh.entity.po.Customer;
import com.wzh.entity.po.Sales;
import com.wzh.entity.po.SalesItem;
import com.wzh.mapper.CompanyMapper;
import com.wzh.mapper.CustomerMapper;
import com.wzh.service.CompanyService;
import com.wzh.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyMapper companyMapper;


    @Override
    public DataResp<Object> findInfo(Long id) {
        DataResp<Object> resp = new DataResp<>();
        Company company = companyMapper.findInfo(id);
        resp.setData(company);
        return resp;
    }

    @Override
    public DataResp<Object> findMyCompany(Long id) {
        DataResp<Object> resp = new DataResp<>();
        List<Company>  companies = companyMapper.findMyCompany(id);
        resp.setData(companies);
        resp.setPageCount(companies.size());
        return resp;
    }

    @Override
    @Transactional
    public DataResp<Object> delMyCompany(Long id) {
        DataResp<Object> resp = new DataResp<>();
        companyMapper.delMyCompany(id);
        companyMapper.delAllemplyees(id);
        return resp;
    }
}
