package com.wzh.service.Impl;

import com.wzh.common.ResultE;
import com.wzh.mapper.UserMapper;


import com.wzh.entity.po.*;
import com.wzh.entity.Resp.DataResp;
import com.wzh.service.UserService;
import com.wzh.util.CommonUtil;
import com.wzh.util.SequenceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;


	@Override
	public DataResp<Object> isExistUserByOpenId(String id) {
		DataResp<Object> resp = new DataResp<>();
		User user =userMapper.isExistUserByOpenId(id);
		if(user==null){
			user=new User();
			user.setOpenId(id);
			user.setUserType("1");
			user.setUserId(SequenceUtil.getSeqIdString());
			userMapper.registerUser(user);
			resp.setCode(ResultE.USERNULL.getCode());
		}else{
			//存在用户信息查询 用户所在的公司或者查看自己公司

			resp.setData(user);
		}
		return resp;
	}

	@Override
	public DataResp<Object> bossRegister(Company company) {
		DataResp<Object> resp = new DataResp<>();
		if (company.getId()==null){
			company.setId(System.currentTimeMillis());
		}
		userMapper.bossRegister(company);
		resp.setData(company);
		return resp;
	}

	@Override
	public DataResp<Object> myCompanyByUserId(String id) {
		DataResp<Object> resp = new DataResp<>();
		 List<Company> company= userMapper.myCompanyByUserId(id);
		 resp.setData(company);
		return resp;
	}
}
