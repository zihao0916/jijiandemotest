package com.wzh.service.Impl;

import cn.hutool.core.date.DateUtil;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.CommonQueryDto;
import com.wzh.entity.po.*;
import com.wzh.entity.vo.CommomVo;
import com.wzh.mapper.EmployeeMapper;
import com.wzh.service.EmployeeService;
import com.wzh.util.TimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class EmployeeServiceIml implements EmployeeService {

	@Resource
	private EmployeeMapper employeeMapper;

	@Override
	public DataResp<Object> selectEmployee(Long id) {
		DataResp<Object> resp = new DataResp<>();
		List<Employee> employees = employeeMapper.selectEmployee(id);
		resp.setData(employees);
		return resp;
	}

	@Override
	public DataResp<Object> MyemployeeByName(Employee employee) {
		DataResp<Object> resp = new DataResp<>();
		List<Employee> employees = employeeMapper.MyemployeeByName(employee);
		resp.setData(employees);
		return resp;
	}

	@Override
	public DataResp<Object> selectMoney(CommonQueryDto commonQueryDto) {
		DataResp<Object> resp = new DataResp<>();
		Map<String,Object> map=new HashMap<>();
		if(commonQueryDto.getTime()!=null){
			//获取开始时间
			Date beginDate = DateUtil.beginOfMonth(commonQueryDto.getTime());
			//获取结束时间
			Date endOfDay = DateUtil.endOfMonth(commonQueryDto.getTime());
			map.put("beginOfMonth",beginDate);
			map.put("endOfMonth",endOfDay);
		}
		map.put("companyId",commonQueryDto.getCompanyId());
		map.put("userName",commonQueryDto.getUserName());
		List<CommomVo> commomVoList= employeeMapper.selectMoney(map);
		resp.setData(commomVoList);
		return resp;
	}

	@Override
	public DataResp<Object> updateInfo(Employee employee) {
		DataResp<Object> resp = new DataResp<>();
		employeeMapper.updateInfo(employee);
		return  resp;
	}

	@Override
	public DataResp<Object> setAdmin(Employee employee) {
		DataResp<Object> resp = new DataResp<>();
		employeeMapper.setAdmin(employee);
		return  resp;
	}

	@Override
	public DataResp<Object> remove(Employee employee) {
		DataResp<Object> resp = new DataResp<>();
		employeeMapper.remove(employee);
		return  resp;
	}

	@Override
	public DataResp<Object> getMyMoneyByTime(CommonQueryDto commonQueryDto) throws ParseException {
		DataResp<Object> resp = new DataResp<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		List<String> timeList= TimeUtil.getDayByMonth(sdf.format(commonQueryDto.getTime()));
		Map<String,Object> map=new HashMap<>();
		Date beginDate = DateUtil.beginOfMonth(commonQueryDto.getTime());
		//获取结束时间
		Date endOfDay = DateUtil.endOfMonth(commonQueryDto.getTime());
		map.put("beginTime",beginDate);
		map.put("endTime",endOfDay);
		map.put("employeeId",commonQueryDto.getEmployeeId());
		List<TaskRecord> taskRecords =employeeMapper.getMyMoneyByTime(map);
		Map<String,Integer> data=new HashMap<>();

			for (String time:timeList
				 ) {
				data.put(time,0);
		    }

		//根据某月的开始时间和结束时间查询所有数据 之后完成匹配
		for (TaskRecord item:taskRecords
			 ) {

			if (data.containsKey(sdf2.format(item.getRecordTime()))){
				System.out.println("重复"+sdf2.format(item.getRecordTime()));
				data.put(sdf2.format(item.getRecordTime()),data.get(sdf2.format(item.getRecordTime()))+item.getNum());

			}
		}


		resp.setData(data);
		return  resp;
	}

	@Override
	public DataResp<Object> getMyMoneyByClothesType(CommonQueryDto commonQueryDto) {
		DataResp<Object> resp = new DataResp<>();
		Map<String,Object> map=new HashMap<>();
		Date beginDate = DateUtil.beginOfMonth(commonQueryDto.getTime());
		//获取结束时间
		Date endOfDay = DateUtil.endOfMonth(commonQueryDto.getTime());
		map.put("beginTime",beginDate);
		map.put("endTime",endOfDay);
		map.put("employeeId",commonQueryDto.getEmployeeId());
		List<TaskRecord> taskRecords =employeeMapper.getMyMoneyByClothesType(map);
		resp.setData(taskRecords);
		return  resp;
	}

	@Override
	public DataResp<Object> register(Employee employee) {
		DataResp<Object> resp = new DataResp<>();
		Company company=employeeMapper.isExist(employee.getCompanyId());
		if(employee.getId()==null){
			employee.setId(System.currentTimeMillis());
		}
		if (company==null){
			resp.setMes("公司不存在！");
			resp.setCode(205);
			return resp;
		}
		employeeMapper.register(employee);
		resp.setData(employee);
		return resp;
	}


}
