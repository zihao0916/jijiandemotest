package com.wzh.service;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.CommonQueryDto;
import com.wzh.entity.po.Employee;
import com.wzh.entity.po.Task;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public interface EmployeeService {
	DataResp<Object> selectEmployee(Long id);

	DataResp<Object> MyemployeeByName(Employee employee);

	DataResp<Object> selectMoney(CommonQueryDto commonQueryDto);


	DataResp<Object> updateInfo(Employee employee);

	DataResp<Object> setAdmin(Employee employee);

	DataResp<Object> remove(Employee employee);

	DataResp<Object> getMyMoneyByTime(CommonQueryDto commonQueryDto) throws ParseException;

	DataResp<Object> getMyMoneyByClothesType(CommonQueryDto commonQueryDto);

	DataResp<Object> register(Employee employee);
}
