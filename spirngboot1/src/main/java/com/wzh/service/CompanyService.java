package com.wzh.service;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.CommonQueryDto;
import com.wzh.entity.po.Employee;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public interface CompanyService {

	DataResp<Object> findInfo(Long id);

	DataResp<Object> findMyCompany(Long id);

	DataResp<Object> delMyCompany(Long id);
}
