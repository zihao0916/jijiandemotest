package com.wzh.service;

import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.Clothes;
import com.wzh.entity.po.ClothesDict;
import com.wzh.entity.po.Task;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.ClotheQueryDto;
import com.wzh.entity.po.TaskRecord;
import org.springframework.stereotype.Service;

@Service
public interface ClothesService {
    DataResp<ClothesDict> sizeAndColor();

    DataResp<Object> newPattern(Clothes clothes);

    DataResp<Object> newTask(ClotheQueryDto clotheQueryDto);

    DataResp<Object> selectClothesByParams(String query);

    DataResp<Object> selectClothes( QueryDto queryDto);

    DataResp<Object> selectClothesTaskById(Long id);

    DataResp<Object> selectClothesSizeAndNum(Long id);

    DataResp<Object> addTaskMount(Task task);

    DataResp<Object> selectClothesAllTask();

    DataResp<Object> selectClothesTaskByQuery( QueryDto queryDto);

    DataResp<Object> selectClothesTaskByClothesId(Long id);

	DataResp<Object> showTaskMount(Task task);

    DataResp<Object> delTaskMount(TaskRecord taskRecord);

    DataResp<Object> delPattern(Long id);

    DataResp<Object> selectClothesInfo(Long id);
}
