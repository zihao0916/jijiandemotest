package com.wzh.service;

import com.wzh.entity.dto.OrderDto;
import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.Customer;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.po.Sales;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    DataResp<Object> newCustomer(Customer customer);

    DataResp<Object> myCustomer(QueryDto queryDto);

    DataResp<Object> newOrder(OrderDto orderDto);

    DataResp<Object> myCustomerOrder(Long id);

    DataResp<Object> payFor(Sales sales);

    DataResp<Object> myCustomerInfo(Long id);

    DataResp<Object> delCustomer(Long id);

    DataResp<Object> myCustomerDetail(Customer customer);
}
