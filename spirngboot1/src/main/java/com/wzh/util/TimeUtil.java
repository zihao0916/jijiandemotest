package com.wzh.util;

import cn.hutool.core.date.DateUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimeUtil {

    public static List<String> getMonthByYear(Integer year) {
        Date date = new Date();
        // 获取当前年份
        if (year == null) {
            year = DateUtil.year(date);
        }
        List<String> month = new ArrayList<>();
        month.add(year + "-01");
        month.add(year + "-02");
        month.add(year + "-03");
        month.add(year + "-04");
        month.add(year + "-05");
        month.add(year + "-06");
        month.add(year + "-07");
        month.add(year + "-08");
        month.add(year + "-09");
        month.add(year + "-10");
        month.add(year + "-11");
        month.add(year + "-12");
        return month;
    }

    /**
     * 获取指定月份所有日期
     *
     * @param yearMonth ("2022-11")
     * @return List<String>
     */
    public static List<String> getDayByMonth(String yearMonth) {
        int year = Integer.parseInt(yearMonth.substring(0, 4));
        int month = Integer.parseInt(yearMonth.substring(5, 7));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        // 清除信息
        cal.clear();
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month - 1);
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int count = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int j = 1; j <= count; j++) {
            fullDayList.add(format.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }

    /**
     * 获取指定月份日期（当前月份到今日为止）
     *
     * @param yearMonth ("2022-11")
     * @return List<String>
     */
    public static List<String> getValidDayByMonth(String yearMonth) {
        int year = Integer.parseInt(yearMonth.substring(0, 4));
        int month = Integer.parseInt(yearMonth.substring(5, 7));
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd");
        SimpleDateFormat format3 = new SimpleDateFormat("MM");
        int nowMonth = Integer.parseInt(format3.format(date));
        int nowDay = Integer.parseInt(format2.format(date));
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        // 清除信息
        cal.clear();
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month - 1);
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int count = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (month == nowMonth) {
            count = nowDay;
        }
        for (int j = 1; j <= count; j++) {
            fullDayList.add(format.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }

    /**
     * 获取最近几天的时间List
     *
     * @param days (7)
     * @return List<String>
     */
    public static List<String> getDaysBetwwen(int days) {
        List<String> dayss = new ArrayList<>();
        Calendar start = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -(days - 1));
        start.setTime(c.getTime());
        long startTime = start.getTimeInMillis();
        Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        long endTime = end.getTimeInMillis();
        long oneDay = 1000 * 60 * 60 * 24L;
        long time = startTime;
        while (time <= endTime) {
            Date d = new Date(time);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(df.format(d));
            dayss.add(df.format(d));
            time += oneDay;
        }
        return dayss;
    }
}
