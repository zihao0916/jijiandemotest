package com.wzh.util;

/**
 * @Author GerryMC
 * @Description 生成雪花id工具类
 *
 * @Date: 2020/5/15 0015 14:02
 */
public class SequenceUtil {
    private static final SnowflakeIdWorker ID_WORKER = new SnowflakeIdWorker(0, 0);

    /**
     * 增加sequenceId值：返回long类型
     *
     * @return Long
     */
    public static Long getSeqId() {
        return ID_WORKER.nextId();
    }

    /**
     * 增加sequenceId值: 返回string类型
     *
     * @return String
     */
    public static String getSeqIdString() {
        return String.valueOf(ID_WORKER.nextId());
    }
}
