package com.wzh.util;


import cn.hutool.core.util.ReflectUtil;


import lombok.extern.slf4j.Slf4j;


import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Slf4j
public class ParamUtil {







    /**
     * 设置创建基础字段
     * - 创建时间：createTime
     * - 创建人：updateTime
     * - 更新时间：createBy
     * - 更新人：updateBy
     * - 删除标记：delFlag
     *
     * @param param    参数
     * @param operator 操作人
     */
    public static void setVideoCreateParam(Object param, Long operator) {
        try {
            Date date = new Date();
            final Field[] fields = ReflectUtil.getFields(param.getClass());
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();
                if ("createTime".equals(fieldName)) {
                    field.set(param, date);
                }
                if ("updateTime".equals(fieldName)) {
                    field.set(param, date);
                }

                if ("isDelete".equals(fieldName)) {
                    field.set(param, 0);
                }
                if ("auditState".equals(fieldName)) {
                    field.set(param, 0);
                }

            }
        } catch (IllegalAccessException  e) {
            log.error("参数设置失败：{}", e.getMessage());
        }
    }
}
