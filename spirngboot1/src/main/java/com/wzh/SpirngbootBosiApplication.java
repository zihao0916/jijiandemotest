package com.wzh;

import lombok.extern.log4j.Log4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@MapperScan("com.wzh.mapper")
@SpringBootApplication
@EnableTransactionManagement
public class SpirngbootBosiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpirngbootBosiApplication.class, args);
		System.out.println("项目启动了！");
	}


//	@Scheduled(cron = "0/2 * * * * ? ")
//	public void closeOrderTaskV1() {
//		System.out.println("定时任务启动");
//	}
@Bean
public ConfigurableServletWebServerFactory webServerFactory() {
	TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
	factory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> connector.setProperty("relaxedQueryChars", "|{}[]\\"));
	return factory;
}

}
