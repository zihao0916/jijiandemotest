package com.wzh.controller;

import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.Clothes;
import com.wzh.entity.po.ClothesDict;
import com.wzh.entity.po.Task;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.ClotheQueryDto;
import com.wzh.entity.po.TaskRecord;
import com.wzh.service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/clothes")
public class ClothesController {
    @Resource
    private ClothesService clothesService;

    //查询所有尺码和颜色字典
    @GetMapping ("/sizeAndColor")
    @ResponseBody
    public DataResp sizeAndColor(){
        DataResp<ClothesDict> resp= clothesService.sizeAndColor();
        return resp;
    };

    //新建款式
    @PostMapping ("/newPattern")
    @ResponseBody
    public DataResp newPattern(@RequestBody Clothes clothes){
        DataResp<Object> resp= clothesService.newPattern(clothes);
        return resp;
    };

    //删除款式
    @GetMapping ("/delPattern/{id}")
    @ResponseBody
    public DataResp delPattern(@PathVariable("id") Long id){
        DataResp<Object> resp= clothesService.delPattern(id);
        return resp;
    };


    //根据id查询衣服信息
    @GetMapping ("/selectClothesInfo/{id}")
    @ResponseBody
    public DataResp selectClothesInfo(@PathVariable("id") Long id){
        DataResp<Object> resp= clothesService.selectClothesInfo(id);
        return resp;
    };

    //编号名称查询衣服
    @GetMapping  ("/selectClothesByParams/{query}")
    @ResponseBody
    public DataResp selectClothesByParams(@PathVariable("query") String query){
        DataResp<Object> resp= clothesService.selectClothesByParams(query);
        return resp;
    };

    //根据菲码编号/款式编号/款式名称查非编任务
    //编号名称查询衣服
    @PostMapping("/selectClothesTaskByQuery")
    @ResponseBody
    public DataResp selectClothesTaskByQuery(@RequestBody QueryDto queryDto){
        DataResp<Object> resp= clothesService.selectClothesTaskByQuery(queryDto);
        return resp;
    };

    //查询自己公司所有衣服款式衣服
    @PostMapping("/selectClothes")
    @ResponseBody
    public DataResp selectClothesByParams(@RequestBody QueryDto queryDto){
        DataResp<Object> resp= clothesService.selectClothes(queryDto);
        return resp;
    };


    //新建非编
    @PostMapping ("/newTask")
    @ResponseBody
    public DataResp newCode(@RequestBody ClotheQueryDto clotheQueryDto){
        DataResp<Object> resp= clothesService.newTask(clotheQueryDto);
        return resp;
    };

    //查询所有非编生产任务
    @GetMapping ("selectClothesTaskById")
    @ResponseBody
    public DataResp selectClothesTaskById(){
        DataResp<Object> resp= clothesService.selectClothesAllTask();
        return resp;
    };




    //根据菲码id查询一条菲码生产任务
    @GetMapping ("selectClothesTaskById/{id}")
    @ResponseBody
    public DataResp allTaskById(@PathVariable("id") Long id){
        DataResp<Object> resp= clothesService.selectClothesTaskById(id);
        return resp;
    };

    //根据衣服id查询所有菲码生产任务
    @GetMapping ("selectClothesTaskByClothesId/{id}")
    @ResponseBody
    public DataResp selectClothesTaskByClothesId(@PathVariable("id") Long id){
        DataResp<Object> resp= clothesService.selectClothesTaskByClothesId(id);
        return resp;
    };






    //根据款式id查询所有库存
    @GetMapping ("selectClothesSizeAndNum/{id}")
    @ResponseBody
    public DataResp selectClothesSizeAndNum(@PathVariable("id") Long id){
        DataResp<Object> resp= clothesService.selectClothesSizeAndNum(id);
        return resp;
    };


    //员工上数
    @PostMapping ("/addTaskMount")
    @ResponseBody
    public DataResp addTaskMount(@RequestBody Task task){
        DataResp<Object> resp= clothesService.addTaskMount(task);
        return resp;
    };


    //查看员工上数
    @PostMapping ("/showTaskMount")
    @ResponseBody
    public DataResp showTaskMount(@RequestBody Task task){
        DataResp<Object> resp= clothesService.showTaskMount(task);
        return resp;
    };


    //查看员工撤回上数
    @PostMapping ("/delTaskMount")
    @ResponseBody
    public DataResp showTaskMount(@RequestBody TaskRecord taskRecord){
        DataResp<Object> resp= clothesService.delTaskMount(taskRecord);
        return resp;
    };

}
