package com.wzh.controller;

import com.wzh.entity.po.*;
import com.wzh.entity.Req.DataReq;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.result;
import com.wzh.service.Impl.UserServiceImpl;
import com.wzh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/User")
public class UserController {
	@Autowired
	private UserService userService;

	//新建公司
	@PostMapping("/bossRegister")
	@ResponseBody
	public DataResp bossRegister(@RequestBody Company company){
		DataResp<Object> resp= userService.bossRegister(company);
		return resp;
	};




	//查询用户信息没有则新建
	@GetMapping("/isExistUserByOpenId/{id}")
	@ResponseBody
	public DataResp isExistUserByOpenId(@PathVariable("id") String id){
		DataResp<Object> resp= userService.isExistUserByOpenId(id);
		return resp;
	}

	//查询自己所在的公司
	@GetMapping("/myCompanyByUserId/{id}")
	@ResponseBody
	public DataResp myCompanyByUserId(@PathVariable("id") String id){
		DataResp<Object> resp= userService.myCompanyByUserId(id);
		return resp;
	}





}
