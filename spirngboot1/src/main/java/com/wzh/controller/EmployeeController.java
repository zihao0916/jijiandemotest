package com.wzh.controller;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.CommonQueryDto;
import com.wzh.entity.po.Clothes;
import com.wzh.entity.po.Employee;
import com.wzh.entity.po.Task;
import com.wzh.service.CustomerService;
import com.wzh.service.EmployeeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Resource
	private EmployeeService employeeService;
	//根据公司id查询我的公司所有员工
	@GetMapping("/myEmployee/{id}")
	@ResponseBody
	public DataResp Myemployee(@PathVariable("id") Long id){
		DataResp<Object> resp= employeeService.selectEmployee(id);
		return resp;
	};


	//查询员工基本信息
	@PostMapping ("/myEmployeeByQuery")
	@ResponseBody
	public DataResp myEmployeeByQuery(@RequestBody Employee employee){
		DataResp<Object> resp= employeeService.MyemployeeByName(employee);
		return resp;
	};

	//查询自己公司员工工资
	@PostMapping ("/selectMoney")
	@ResponseBody
	public DataResp newPattern(@RequestBody CommonQueryDto commonQueryDto){
		DataResp<Object> resp= employeeService.selectMoney(commonQueryDto);
		return resp;
	};

	//员工注册
	@PostMapping ("/register")
	@ResponseBody
	public DataResp register(@RequestBody Employee employee){
		DataResp<Object> resp= employeeService.register(employee);
		return resp;
	};


	//员工修改信息
	@PostMapping ("/updateInfo")
	@ResponseBody
	public DataResp updateInfo(@RequestBody Employee employee){
		DataResp<Object> resp= employeeService.updateInfo(employee);
		return resp;
	};

	//设置员工为管理员
	@PostMapping ("/setAdmin")
	@ResponseBody
	public DataResp setAdmin(@RequestBody Employee employee){
		DataResp<Object> resp= employeeService.setAdmin(employee);
		return resp;
	};

	//移除员工
	@PostMapping ("/remove")
	@ResponseBody
	public DataResp remove(@RequestBody Employee employee){
		DataResp<Object> resp= employeeService.remove(employee);
		return resp;
	};


	//员工查看自己的工资 按时间
	@PostMapping ("/getMyMoneyByTime")
	@ResponseBody
	public DataResp getMyMoneyByTime(@RequestBody CommonQueryDto commonQueryDto) throws ParseException {
		DataResp<Object> resp= employeeService.getMyMoneyByTime(commonQueryDto);
		return resp;
	};


	//员工查看自己的工资 按款式
	@PostMapping ("/getMyMoneyByClothesType")
	@ResponseBody
	public DataResp getMyMoneyByClothesType(@RequestBody CommonQueryDto commonQueryDto)  {
		DataResp<Object> resp= employeeService.getMyMoneyByClothesType(commonQueryDto);
		return resp;
	};
}
