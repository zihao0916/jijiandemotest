package com.wzh.controller;

import com.wzh.entity.dto.ClotheQueryDto;
import com.wzh.entity.dto.OrderDto;
import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.Customer;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.po.Sales;
import com.wzh.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Resource
    private CustomerService customerService;
    //添加/修改客户
    @PostMapping
    @ResponseBody
    public DataResp customer(@RequestBody Customer customer){
        DataResp<Object> resp= customerService.newCustomer(customer);
        return resp;
    };

    //删除我的客户
    @GetMapping("/delCustomer/{id}")
    @ResponseBody
    public DataResp delCustomer(@PathVariable("id") Long id){
        DataResp<Object> resp= customerService.delCustomer(id);
        return resp;
    };

    //查看我的客户详细信息
    @GetMapping("/myCustomerInfo/{id}")
    @ResponseBody
    public DataResp myCustomerInfo(@PathVariable("id") Long id){
        DataResp<Object> resp= customerService.myCustomerInfo(id);
        return resp;
    };


    //查看我的客户详细信息
    @PostMapping("/myCustomerDetail")
    @ResponseBody
    public DataResp myCustomerDetail(@RequestBody Customer customer){
        DataResp<Object> resp= customerService.myCustomerDetail(customer);
        return resp;
    };

    //查看我的客户
    @PostMapping("/myCustomer")
    @ResponseBody
    public DataResp myCustomer(@RequestBody QueryDto queryDto){
        DataResp<Object> resp= customerService.myCustomer(queryDto);
        return resp;
    };


    //根据客户id查询客户订单信息
    @GetMapping ("/myCustomerOrder/{id}")
    @ResponseBody
    public DataResp myCustomerOrder(@PathVariable("id") Long id){
        DataResp<Object> resp= customerService.myCustomerOrder(id);
        return resp;
    };

    //新建客户订单
    @PostMapping ("/newOrder")
    @ResponseBody
    public DataResp newCode(@RequestBody OrderDto orderDto){
        DataResp<Object> resp= customerService.newOrder(orderDto);
        return resp;
    };


    //向客户收款
    @PostMapping ("/payFor")
    @ResponseBody
    public DataResp payFor(@RequestBody Sales sales){
        DataResp<Object> resp= customerService.payFor(sales);
        return resp;
    };



}
