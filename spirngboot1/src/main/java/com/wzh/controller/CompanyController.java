package com.wzh.controller;

import com.wzh.entity.Resp.DataResp;
import com.wzh.service.CompanyService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/company")
public class CompanyController {

	@Resource
	private CompanyService companyService;

	//
	@GetMapping("/findInfo/{id}")
	@ResponseBody
	public DataResp delCustomer(@PathVariable("id") Long id){
		DataResp<Object> resp= companyService.findInfo(id);
		return resp;
	};
	@GetMapping("/findMyCompany/{id}")
	@ResponseBody
	public DataResp findMyCompany(@PathVariable("id") Long id){
		DataResp<Object> resp= companyService.findMyCompany(id);
		return resp;
	};

	@GetMapping("/delMyCompany/{id}")
	@ResponseBody
	public DataResp delMyCompany(@PathVariable("id") Long id){
		DataResp<Object> resp= companyService.delMyCompany(id);
		return resp;
	};
}
