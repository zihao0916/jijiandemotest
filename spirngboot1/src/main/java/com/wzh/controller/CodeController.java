package com.wzh.controller;

import com.wzh.service.Impl.QRServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class CodeController {
    @Resource
    QRServiceImpl qrService;
    @RequestMapping("code")
    public void generateV3(String content, HttpServletResponse servletResponse) throws IOException, IOException {
        qrService.generateStream(content,servletResponse);
    }

}
