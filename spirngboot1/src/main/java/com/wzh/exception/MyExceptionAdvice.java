package com.wzh.exception;


import com.wzh.entity.Resp.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@ControllerAdvice(basePackages = {"com.wzh.controller"})
public class MyExceptionAdvice {

    private static final Logger logger = LoggerFactory.getLogger(MyExceptionAdvice.class);

    /**
     * 处理与用户相关的业务异常
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseResult UserExceptionHandler(HttpServletRequest request, MyException e){
        logger.info("业务异常：Host:{} invoke URL:{},错误信息：{}",request.getRemoteHost(),request.getRequestURL(),e.getMessage());
        return new ResponseResult(e.getCode(),e.getMessage());
    }

}
