package com.wzh.mapper;

import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.Customer;
import com.wzh.entity.po.Sales;
import com.wzh.entity.po.SalesItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface CustomerMapper {
    void newCustomer(Customer customer);

    List<Customer> selectMyCustomer(QueryDto queryDto);

    void newOrder(@Param("map") HashMap<String, Object> map);

    void newOrderItem(@Param("entities") List<SalesItem>  entities);

    List<Sales> findSales(@Param("id")  Long id);

    List<SalesItem> findSalesItemById(@Param("id") Long id);


    void payFor(Sales sales);

    Customer myCustomerInfo(@Param("id") Long id);

    void delCustomer(@Param("id") Long id);

    Integer selectMyCustomerCount(QueryDto queryDto);

    List<Customer> selectMyCustomerDetail(Customer customer);
}
