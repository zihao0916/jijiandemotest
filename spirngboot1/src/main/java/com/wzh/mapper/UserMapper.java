package com.wzh.mapper;

import com.wzh.entity.po.*;
import com.wzh.entity.Req.DataReq;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface UserMapper {



    @Select("select open_id openId,user_id userId,user_type userType from uc_user where open_id=#{id}")
	User isExistUserByOpenId(@Param("id") String id);

	//@Insert("insert into oa_company (id,name,address,owner_user_id) value (#{id},#{name},#{address},#{ownerUserId})")

	void bossRegister(Company company);
	@Insert("insert into uc_user (open_id,user_id,user_type) value (#{openId},#{userId},#{userType})")
	void registerUser(User user);

	List<Company> myCompanyByUserId(@Param("id") String id);
}
