package com.wzh.mapper;

import com.wzh.entity.dto.QueryDto;
import com.wzh.entity.po.*;
import com.wzh.entity.vo.ClothesInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ClothesMapper {
    @Select("select id,color_name colorName from pie_color")
    List<Color> findColor();
    @Select("select id,size_name sizeName from pie_size")
    List<Size> findSize();


    int newPattern(Clothes clothes);

    Integer newCode( @Param("entities") List<ClothesInfo> entities);

    List<Clothes> selectClothesByParams(String query);

    List<Clothes> selectClothes(QueryDto queryDto);

    ClothesInfoVo selectClothesTaskById(@Param("id") Long id);

    List<colorAndNum> findColorAndNum(@Param("id")  Long id);

    List<SizeAndNum> getSizeAndNum(colorAndNum colorAndNum);

    void addTaskMount(Task task);

    void addTaskRecord(Task task);

    List<ClothesInfoVo> selectClothesAllTask();

    List<ClothesInfoVo> selectClothesTaskByQuery( QueryDto queryDto);

    List<ClothesInfo> selectClothesTaskByClothesId(Long id);

	TaskRecord showTaskMount(Task task);

    void delTaskMount(TaskRecord taskRecord);

    void delPattern(@Param("id") Long id);

    Integer findCount(QueryDto queryDto);

    Integer selectClothesCount(QueryDto queryDto);

    void delTaskMountById(@Param("taskId") Long id,@Param("num") Integer realNum);

    Clothes selectClothesInfo(@Param("id") Long id);
}
