package com.wzh.mapper;

import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.po.Company;
import com.wzh.entity.po.Employee;
import com.wzh.entity.po.TaskRecord;
import com.wzh.entity.vo.CommomVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface CompanyMapper {


	Company  findInfo(@Param("id") Long id);

	List<Company> findMyCompany(@Param("id") Long id);

	void delMyCompany(@Param("id") Long id);

	void delAllemplyees(@Param("id") Long id);
}
