package com.wzh.mapper;

import com.wzh.entity.Req.DataReq;
import com.wzh.entity.Resp.DataResp;
import com.wzh.entity.dto.CommonQueryDto;
import com.wzh.entity.po.*;
import com.wzh.entity.vo.CommomVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface EmployeeMapper {

	List<Employee> selectEmployee(Long id);

	List<Employee> MyemployeeByName(Employee employee);

	List<CommomVo> selectMoney(@Param("query") Map<String, Object> map);


	void updateInfo(Employee employee);

	void setAdmin(Employee employee);

	void remove(Employee employee);

	List<TaskRecord> getMyMoneyByTime(@Param("query") Map<String, Object> map);

	List<TaskRecord> getMyMoneyByClothesType(@Param("query") Map<String, Object> map);

	Company isExist(Long companyId);

	void register(Employee employee);
}
