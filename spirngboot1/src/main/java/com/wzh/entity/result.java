package com.wzh.entity;

public class result {
	private String mes;
	private Integer code;
	private Integer pageCount;
	private Object data;


	public result(String mes, Integer code, Object data,Integer pageCount) {
		this.mes = mes;
		this.code = code;
		this.data = data;
		this.pageCount = pageCount;
	}
	public result(String mes, Integer code, Object data) {
		this.mes = mes;
		this.code = code;
		this.data = data;
	}
	@Override
	public String toString() {
		return "result{" +
				"mes='" + mes + '\'' +
				", code=" + code +
				", pageCount=" + pageCount +
				", data=" + data +
				'}';
	}

	public result(String mes, Integer code) {
		this.mes = mes;
		this.code = code;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
