package com.wzh.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class ClotheQueryDto {

    private Long clothesId;
    private Long companyId;
    private List<ColorsDto> colorsDtos;
}
