package com.wzh.entity.dto;

import lombok.Data;

@Data
public class CustomerDto {
    private Long id;
    private String name;//客户名称
    private String linkman;//联系人
    private String phone;//手机号码
    private double receivableAmount;//应收
    private double paidAmount;//已付
    private Long companyId;//客户归属id

}
