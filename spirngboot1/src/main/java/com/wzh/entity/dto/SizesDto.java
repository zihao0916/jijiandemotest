package com.wzh.entity.dto;

import lombok.Data;

@Data
public class SizesDto {
    private String sizeName;
    private Integer mount;
    private Long sizeId;

}
