package com.wzh.entity.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OrderDto {
    private Date salesTime;//交货时间
    private double receivableAmount;//应收
    private double paidAmount;//已付
    private Long companyId;//客户归属id
    private Long customerId;//客户id
    private Long clothesId;
    private List<ColorsDto> colorsDtos;
}
