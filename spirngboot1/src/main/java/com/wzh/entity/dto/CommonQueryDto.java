package com.wzh.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class CommonQueryDto {
  private 	String userName;
  @JsonFormat(pattern = "yyyy-MM")
  private Date time;
  private Long companyId;
  private Long employeeId;
  private String mouth;
}
