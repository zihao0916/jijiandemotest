package com.wzh.entity.dto;

import lombok.Data;

@Data
public class QueryDto {
    private Long companyId;
    private Long id;
    private String query;
    private Integer index;
    private Integer count;
}
