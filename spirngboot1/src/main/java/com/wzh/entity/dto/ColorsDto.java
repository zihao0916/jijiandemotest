package com.wzh.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class ColorsDto {

    private Long colorId;
    private String colorName;
    private List<SizesDto> sizeDtos;

}
