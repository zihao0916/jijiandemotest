package com.wzh.entity.Resp;

import com.wzh.common.ResultE;
import lombok.Data;

@Data
public class DataResp<T> {
	private String mes=ResultE.SUCCESS.getMsg();//返回消息
	private Integer code=ResultE.SUCCESS.getCode();//状态码
	private Integer pageCount=0;//数据总数
	private T data;//装载的数据
}
