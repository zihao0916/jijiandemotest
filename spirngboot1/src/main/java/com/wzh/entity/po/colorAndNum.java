package com.wzh.entity.po;

import lombok.Data;

import java.util.List;

@Data
public class colorAndNum {

    private Long colorId;
    private Long clothesId;
    private Integer num;
    private Integer realNum;
    private String colorName;

    List<SizeAndNum> sizeAndNumList;
}
