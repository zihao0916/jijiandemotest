package com.wzh.entity.po;

import lombok.Data;
import java.util.List;

@Data
public class RoleType {
	private Integer rid;
	private String rname;
	private List<Treemenu>  tree;
}
