package com.wzh.entity.po;

import lombok.Data;

@Data
public class Color {
    private Integer id;
    private String colorName;
}
