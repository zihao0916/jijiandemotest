package com.wzh.entity.po;

import lombok.Data;

import java.util.Date;

@Data
public class Task {
    private Long employeeId;
    private Long companyId;
    private Long taskId;
    private Integer num;
    private Date recordTime;
}
