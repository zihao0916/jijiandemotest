package com.wzh.entity.po;

import lombok.Data;

@Data
public class Clothes {
    private Long companyId;
    private Long id;
    private String clothesName;
    private String clothesEncode;
    private int clothesNum;
    private String pic;

}
