package com.wzh.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class TaskRecord {
	private Long id;
	private Integer num;
	private Long employeeId;
	private Long companyId;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
   private Date recordTime;
   private Long taskId;
   private String clothesName;
   private String sizeName;
   private String colorName;
}
