package com.wzh.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ClothesInfo {
    private Long id;
    private Long companyId;
    private Long clothesId;
    private Long sizeId;
    private String sizeName;
    private Long colorId;
    private String colorName;
    private Integer mount;
    private Integer num;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
