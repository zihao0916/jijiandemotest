package com.wzh.entity.po;

import lombok.Data;

import java.util.Date;

@Data
public class Customer {
    private Long id;
    private String name;//客户名称
    private String linkman;//联系人
    private String phone;//手机号码
    private String address;//地址
    private double receivableAmount;//应收
    private double paidAmount;//已付
    private Long companyId;//客户归属id

    private Integer is_deleted;
    private Date createdTime;
    private Long createdId;
    private String createdName;
    private Date modifiedTime;
    private Long modifiedId;
    private String modifiedName;
}
