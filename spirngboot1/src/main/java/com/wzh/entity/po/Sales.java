package com.wzh.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Sales {
    private Long id;
    private Long customerId;
    private Long companyId;
    private Double receivableAmount;
    private Double paidAmount;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date salesTime;
    private List<SalesItem> salesItemList;
    private StringBuilder html;
    private Clothes clothes;
}
