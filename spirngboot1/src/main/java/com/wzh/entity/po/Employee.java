package com.wzh.entity.po;

import lombok.Data;

@Data
public class Employee {
	private Long id;
	private String userId;
	private String userName;
	private String userNumber;
	private String phone;
	private Integer sex;
	private String userType;
	private Long companyId;
	
}
