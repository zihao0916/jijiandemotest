package com.wzh.entity.po;


import lombok.Data;

@Data
public class ClothesCode {
    private Integer id;
    private Integer clothesId;
    private Integer infoId;

}
