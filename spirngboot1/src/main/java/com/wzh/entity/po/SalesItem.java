package com.wzh.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class SalesItem {
    private Long id;
    private Long salesId;
    private Long clothesId;
    private Long companyId;
    private Integer num;
    private String colorName;
    private String sizeName;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Date saleTime;
}
