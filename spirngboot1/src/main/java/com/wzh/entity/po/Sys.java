package com.wzh.entity.po;

import lombok.Data;

@Data
public class Sys {
	private Integer id;
	private String name;
	private String value;
}
