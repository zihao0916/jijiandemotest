package com.wzh.entity.po;

import lombok.Data;

@Data
public class Treemenu {
	private Integer tid;
	private String tname;
	private String t_class;
	private String label;
	private String path;

	
}
