package com.wzh.entity.po;

import lombok.Data;

@Data
public class Size {
    private Integer id;
    private String sizeName;
}
