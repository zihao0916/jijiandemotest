package com.wzh.entity.po;


import lombok.Data;

@Data
public class ClothesImg {
    private Integer id;
    private String url;
    private Integer clothesId;
}
