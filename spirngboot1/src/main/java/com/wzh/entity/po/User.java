package com.wzh.entity.po;

import lombok.Data;

@Data
public class User {

	private String userId;
	private String openId;
	private String userType;

}
