package com.wzh.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class SizeAndNum {

    private  Long id;//Task ID
    private String sizeName;
    private Integer realNum;


    private String colorName;
    private Long colorId;
    private Long sizeId;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
