package com.wzh.entity.po;

import lombok.Data;

import java.util.List;

@Data
public class ClothesDict {

    private List<Color> colors;
    private List<Size> sizes;

}
