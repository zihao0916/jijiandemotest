package com.wzh.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ClothesInfoVo {
    private Long id;//菲码编号
    private Long clothesId;//款式编号
    private Long sizeId;
    private Long colorId;
    private String sizeName;
    private String colorName;
    private Integer mount;
    private String clothesName;
    private String clothesEncode;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
