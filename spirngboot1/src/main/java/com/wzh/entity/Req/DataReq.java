package com.wzh.entity.Req;

import lombok.Data;

@Data
public class DataReq {

	private String orderId;//userid
	private String userid;//userid
	private Integer pageIndex=0;	//当前位于第几个
	private Integer pageCount=20;//一页显示多少个
}
